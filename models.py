import peewee

db = peewee.SqliteDatabase('data.db')

class User(peewee.Model):
    tid = peewee.IntegerField()

    class Meta:
        database = db


class Person(peewee.Model):
    user = peewee.ForeignKeyField(User)
    name = peewee.CharField()
    birthday = peewee.DateField()

    class Meta:
        database = db

db.connect()
db.create_tables([User, Person])