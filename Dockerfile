FROM python:3.7.3-slim
RUN pip install pytelegrambotapi pysocks gunicorn requests[socks]
ADD src .
CMD ["python", "bot.py"]