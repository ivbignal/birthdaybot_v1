from telegram.ext import Updater, CommandHandler, CallbackContext, MessageHandler, Filters
from telegram import Update

class OrangeBot:
    def __init__(self):
        self.updater = Updater('1652354042:AAF1oodkIbM9nFZKXYhlaAMgOo65raysCz4')

        self.dispatcher = self.updater.dispatcher

        self.dispatcher.add_handler( CommandHandler('start', self.Start) )
        self.dispatcher.add_handler( MessageHandler(Filters.all, self.Start) )
    
    def run(self):
        self.updater.start_polling()
        self.updater.idle()
    
    def Start(self, update: Update, context: CallbackContext):
        print(update)
        try:
            if update.message.text == 'Pineapple':
                update.message.reply_to_message.edit_text('Pineapple! 🍍')
            else:
                update.message.reply_to_message.edit_text('Apple! 🍏')
        except AttributeError:
            try:
                update.message.reply_text('Orange! 🍊')
            except AttributeError:
                update.edited_message.reply_text('Orange! 🍊')

if __name__ == "__main__":
    bot = OrangeBot()
    bot.run()
