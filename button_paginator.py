import os

def draw_keyboard(obj, page=0, objects_by_page=5, prev_next=True):
    os.system('clear')
    for i in obj[objects_by_page*page:objects_by_page*page+objects_by_page]:
        pl = ' ' * int((20 - len(i)) / 2)
        print(f'{pl}{i}')
    if prev_next:
        print('<< Prev      Next >>')
    

objects = ['Трактор', 'Дрезина', 'Гималай', 'Демократ', 'Борзун', 'Новожила', 'Зюзя', 'Паркун', 'Акакий', 'Евлампий', 'Христодула', 'Кончита', 'Дайсуке', 'Путин', 'Солнышко', 'Луна']


page = 0
while True:
    draw_keyboard(objects, page=page)
    inp = input()
    if inp == 'n':
        page += 1
        draw_keyboard(objects, page=page)
    if inp == 'p':
        page -= 1
        draw_keyboard(objects, page=page)
