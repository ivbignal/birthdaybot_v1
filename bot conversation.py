from operator import pos
from time import sleep
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater, Defaults, CallbackContext, CommandHandler, MessageHandler, ConversationHandler, Filters, CallbackQueryHandler, Handler
from models import User, Person
import schedule
import threading
import time
from datetime import date



class ST:
    ASKNAME, ASKPOSITION, CONFIRM, FINISH = range(4)
    CONFIRM_INLINE_KEYBOARD = [
        [
            InlineKeyboardButton("ДА", callback_data='chat_yes'),
            InlineKeyboardButton("НЕТ", callback_data='chat_no'),
        ]
    ]
    CANCEL_INLINE_KEYBOARD = [
        [
            InlineKeyboardButton("ОТМЕНИТЬ ВСЕ", callback_data='chatcancel'),
        ]
    ]


class BirthdayBot:
    chat = {}

    def __init__(self):
        self.bot = Updater('2133566678:AAGij55FbZuibZ6QVyeDn6v2kgOJ7DJ2Zgg')
        
        self.dispatcher = self.bot.dispatcher

        self.chat_handler = ConversationHandler(
            entry_points=[CommandHandler('chat', self.chat_init)],
            states={
                ST.ASKNAME: [MessageHandler(Filters.all, self.chat_askname)],
                ST.ASKPOSITION: [MessageHandler(Filters.all, self.chat_askposition)],
                ST.CONFIRM: [CallbackQueryHandler(self.chat_confirm, pattern=r'^chat_.*$')],
            },
            fallbacks=[CallbackQueryHandler(self.chat_cancel, pattern=r'^chatcancel$')]
        )

        self.dispatcher.add_handler(self.chat_handler)

        self.run()

    def chat_init(self, update:Update, context:CallbackContext):
        self.chat[update.message.from_user.id] = {}
        update.message.reply_text(
            'Ок, новый сотрудник! Как его зовут? (ФИО)',
            reply_markup=InlineKeyboardMarkup(ST.CANCEL_INLINE_KEYBOARD, one_time_keyboard=False)
        )
        return ST.ASKNAME

    def chat_askname(self, update:Update, context:CallbackContext):
        employee = update.message.text
        self.chat[update.message.from_user.id]['name'] = employee
        update.message.reply_text(
            f'Принял! На какой должности работает {employee}?',
        )
        return ST.ASKPOSITION

    def chat_askposition(self, update:Update, context:CallbackContext):
        employee = self.chat[update.message.from_user.id]['name']
        position = update.message.text
        self.chat[update.message.from_user.id]['position'] = position
        update.message.reply_text(
            f'Итак!\n\nНовый сотрудник {employee}, работает на должности {position}\n\nВсе верно?',
            reply_markup=InlineKeyboardMarkup(ST.CONFIRM_INLINE_KEYBOARD, one_time_keyboard=False)
        )
        return ST.CONFIRM

    def chat_confirm(self, update:Update, context:CallbackContext):
        answer = update.callback_query.data
        if answer == 'chat_yes':
            update.callback_query.message.reply_text(
                'Ну все, записали!'
            )
            return ConversationHandler.END
        else:
            update.callback_query.message.reply_text(
                'Ну давай по новой! Как его зовут? (ФИО)'
            )
            return ST.ASKNAME

    def chat_cancel(self, update:Update, context:CallbackContext):
        update.callback_query.message.reply_text(
            'Ок. Забыли. Это останется между нами...'
        )
        return ConversationHandler.END

    def chat_finish(self, update:Update, context:CallbackContext):
        update.message.reply_text(
            'Угу...',
        )
        return ConversationHandler.END

    def run(self):
        self.bot.start_polling()
        self.bot.idle()

if __name__ == "__main__":
    bot = BirthdayBot()
    bot.run()