import logging
from logging import info
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename='dev.log')


info('Loading dependencies...')
from time import sleep
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater, Defaults, CallbackContext, CommandHandler, MessageHandler, ConversationHandler, Filters, CallbackQueryHandler, Handler
from models import User, Person
import schedule
import threading
import time
from datetime import date

default_conf = Defaults(
    parse_mode='MarkdownV2'
)

def GetLocalUserId(update:Update):
    try:
        tid = update.message.from_user.id
    except AttributeError:
        tid = update.callback_query.message.from_user.id
    try:
        uid = User.select().where(User.tid == int(tid)).get()
        info(f'Got message from existing user, uid = {uid}')
        return uid
    except:
        user = User(tid=tid)
        uid = user.save()
        info(f'Got message from new user, new uid = {uid}')
        return uid


class ST:
    NAME, BIRTHDATE = range(2)
    PERSON_NAME, PERSON_PARTITION = range(2)
    MAIN_KEYBOARD_ADD = 'Добавить человека'
    MAIN_KEYBOARD_ADD_TEACHER = 'Добавить пользователя'
    MAIN_KEYBOARD_LIST = 'Мои люди'
    MAIN_KEYBOARD = [[MAIN_KEYBOARD_ADD, MAIN_KEYBOARD_LIST]]
    CANCEL_KEYBOARD_INLINE = [[InlineKeyboardButton("❌ Отмена", callback_data='cancel')]]


class BirthdayBot:
    new_persons = {}

    def __init__(self):
        info('Creating Bot instance...')
        self.bot = Updater('1576028058:AAHJiYGYqEdS76RGU2SWUy5Jy15-adehhG8', defaults=default_conf)
        
        self.dispatcher = self.bot.dispatcher

        self.newperson_handler = ConversationHandler(
            entry_points=[CommandHandler('newperson', self.NewPerson_init), MessageHandler(Filters.text(ST.MAIN_KEYBOARD_ADD), self.NewPerson_init)],
            fallbacks=[CommandHandler('cancel', self.NewPerson_cancel)],
            states={
                ST.NAME: [MessageHandler(Filters.all, self.NewPerson_name)],
                ST.BIRTHDATE: [MessageHandler(Filters.regex(r'^(\d{1,2}[.]\d{1,2}[.]\d{2,4})$'), self.NewPerson_birthdate)],
            },
        )

        self.newteacher_handler = ConversationHandler(
            entry_points=[CommandHandler('newteacher', self.NewTeacher_init), MessageHandler(Filters.text(ST.MAIN_KEYBOARD_ADD_TEACHER), self.NewTeacher_init)],
            fallbacks=[CommandHandler('cancel', self.NewPerson_cancel)],
            states={
                ST.PERSON_NAME: [MessageHandler(Filters.all, self.NewTeacher_name)],
                ST.PERSON_PARTITION: [MessageHandler(Filters.all, self.NewTeacher_partition)],
            },
        )

        self.dispatcher.add_handler(CommandHandler('start', self.Start))
        self.dispatcher.add_handler(MessageHandler(Filters.text(ST.MAIN_KEYBOARD_LIST), self.ListPersons))
        self.dispatcher.add_handler(CallbackQueryHandler(self.cancelConversation, pattern=r'^cancel$'))
        self.dispatcher.add_handler(self.newperson_handler)
        self.dispatcher.add_handler(self.newteacher_handler)

        self.scheduler = threading.Thread(target=self.StartScheduling)
        self.scheduler.start()

    def run(self):
        info('Starting bot...')
        self.bot.start_polling()
        self.bot.idle()

    def ListPersons(self, update:Update, context:CallbackContext):
        uid = GetLocalUserId(update)
        info(f'[INIT] Listing persons of user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        lst = []
        for el in Person.select().where(Person.user==uid):
            name = el.name
            birthday = "\.".join(str(el.birthday).split("-"))
            lst.append(f'{name} \({birthday}\)')
        lst = '\n'.join(lst)
        update.message.reply_text(
            lst,
            reply_markup=ReplyKeyboardMarkup(ST.MAIN_KEYBOARD, one_time_keyboard=False),
        )
        # return ST.NAME

    def NewTeacher_init(self, update:Update, context:CallbackContext):
        # uid = GetLocalUserId(update)
        # self.new_persons[uid] = []
        info(f'[INIT] Adding new person to user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        msg = update.message.reply_text(
            'Давайте добавим еще одного пользователя\!\n'
            'Отправьте /cancel чтобы отменить добавление\.\n\n',
            # reply_markup=ReplyKeyboardRemove()
            reply_markup=InlineKeyboardMarkup(ST.CANCEL_KEYBOARD_INLINE)
        )
        self.bot.bot.send_message(update.message.chat_id, 
            'Отправьте имя пользователя',
            # reply_markup=ReplyKeyboardRemove()
            reply_markup=ReplyKeyboardRemove()
        )
        return ST.PERSON_NAME

    def NewTeacher_name(self, update:Update, context:CallbackContext):
        # uid = GetLocalUserId(update)
        info(f'[NAME] "{update.message.text}" Adding new person to user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        # self.new_persons[uid].append(update.message.text)
        update.message.reply_text(
            'Отлично\!\n\n'
            'Теперь отправьте должность пользователя'
        )
        return ST.PERSON_PARTITION

    def NewTeacher_partition(self, update:Update, context:CallbackContext):
        # uid = GetLocalUserId(update)
        info(f'[NAME] "{update.message.text}" Adding new person to user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        # self.new_persons[uid].append(update.message.text)
        update.message.reply_text(
            'Отлично\!\n\n'
            'Пользователь добавлен\!'
        )
        return ConversationHandler.END

    def NewPerson_init(self, update:Update, context:CallbackContext):
        uid = GetLocalUserId(update)
        self.new_persons[uid] = []
        info(f'[INIT] Adding new person to user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        msg = update.message.reply_text(
            'Давайте добавим еще одного человека\!\n'
            'Отправьте /cancel чтобы отменить добавление\.\n\n',
            # reply_markup=ReplyKeyboardRemove()
            reply_markup=InlineKeyboardMarkup(ST.CANCEL_KEYBOARD_INLINE)
        )
        self.bot.bot.send_message(update.message.chat_id, 
            'Как зовут вашего товарища?',
            # reply_markup=ReplyKeyboardRemove()
            reply_markup=ReplyKeyboardRemove()
        )
        return ST.NAME

    def NewPerson_name(self, update:Update, context:CallbackContext):
        uid = GetLocalUserId(update)
        info(f'[NAME] "{update.message.text}" Adding new person to user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        self.new_persons[uid].append(update.message.text)
        update.message.reply_text(
            'Отлично\!\n\n'
            'Теперь скажите мне, когда его день рождения?\n'
            'Например: 14\.08\.1987'
        )
        return ST.BIRTHDATE
        
    def NewPerson_birthdate(self, update:Update, context:CallbackContext):
        uid = GetLocalUserId(update)
        info(f'[BIRTHDATE] "{update.message.text}" Adding new person to user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        self.new_persons[uid].append(update.message.text)
        year = int(self.new_persons[uid][1].split('.')[2])
        month = int(self.new_persons[uid][1].split('.')[1])
        day = int(self.new_persons[uid][1].split('.')[0])
        person = Person(user=uid, name=self.new_persons[uid][0], birthday=date(year, month, day))
        person.save()
        update.message.reply_text(
            'Отлично\!\n'
            f'Я добавил напоминание о дне рождения {self.new_persons[uid][0]}', 
            reply_markup=ReplyKeyboardMarkup(ST.MAIN_KEYBOARD, one_time_keyboard=False),
        )

        return ConversationHandler.END
        
    def NewPerson_cancel(self, update:Update, context:CallbackContext):
        # uid = GetLocalUserId(update)
        try:
            info(f'[CANCEL] Adding new person to user { update.callback_query.message.from_user.first_name } { update.callback_query.message.from_user.last_name } ({ update.callback_query.message.from_user.id })')
            update.callback_query.message.reply_text(
                'Окей, забыли\!',
                reply_markup=ReplyKeyboardMarkup(ST.MAIN_KEYBOARD, one_time_keyboard=False),
            )
        except AttributeError:
            info(f'[CANCEL] Adding new person to user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
            update.message.reply_text(
                'Окей, забыли\!',
                reply_markup=ReplyKeyboardMarkup(ST.MAIN_KEYBOARD, one_time_keyboard=False),
            )
        self.newperson_handler.update_state(ConversationHandler.END)
        return ConversationHandler.END
        
    def cancelConversation(self, update:Update, context:CallbackContext):
        # uid = GetLocalUserId(update)
        info(f'[CANCEL] Adding new person to user { update.callback_query.message.from_user.first_name } { update.callback_query.message.from_user.last_name } ({ update.callback_query.message.from_user.id })')
        update.callback_query.message.reply_text(
            'Окей, забыли\!',
            reply_markup=ReplyKeyboardMarkup(ST.MAIN_KEYBOARD, one_time_keyboard=False),
        )

        return ConversationHandler.END
        
    
    def Start(self, update:Update, context:CallbackContext):
        uid = GetLocalUserId(update)
        info(f'Got start message from { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        update.message.reply_text(
            '*Привет\!*\n\n'
            'Я могу напомнить тебе о днях рождения твоих друзей\!',
            reply_markup=ReplyKeyboardMarkup(ST.MAIN_KEYBOARD, one_time_keyboard=False),
        )

    def NotifyUsers(self):
        info('Performing user notification task...')
        for user in User.select():
            for person in Person.select().where(Person.user == user):
                print(user, '  -  ', person)

    def StartScheduling(self):
        info('Initializing scheduler...')
        schedule.every().day.at('10:00:00').do(self.NotifyUsers)
        # schedule.every(10).seconds.do(self.NotifyUsers)
        while True:
            schedule.run_pending()
            time.sleep(1)

if __name__ == "__main__":
    bot = BirthdayBot()
    bot.run()